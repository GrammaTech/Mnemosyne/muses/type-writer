import ast
from collections import defaultdict
from src.ty_tokenizer import get_code_tokens, get_docstring_tokens
from src.util import norm
from src.static_inf_holder import StaticInformationHolder, ArgumentInfo, ReturnInfo

class StaticCollater(ast.NodeVisitor):
    '''
    An AST visitor class that collates the static information from the AST source
    '''
    def __init__(self, source):
       self.source_file = source
       self.current_info = None
       self.arg_status_list = []
       self.seq_map = defaultdict(list)
       self.static_map = defaultdict(list)
       self.ret_error_locs = []

    def get_static_info(self):
        '''
        Returns the static map containing both open and closed types
        '''
        return self.static_map

    def _helper_visit_arg(self, node):
        '''
        A helper functions for getting the argument names
        '''
        if node.annotation is not None:
            with open(self.source_file, 'r') as s_file:
                source = s_file.read()
                ty = str(ast.get_source_segment(source, node.annotation))

        else:
            ty = None
        self.arg_status_list.append((node.arg, ty, node.lineno, node.col_offset))
    def visit_Name(self, node):
        '''
        Get the usage sequences at each Name node
        '''
        token_values = get_code_tokens(node, [arg_tuple[0] for arg_tuple in self.arg_status_list], self.source_file)
        if token_values:
            self.seq_map[node.id].append(list(token_values))
        self.generic_visit(node)

    def visit_Return(self, node):
        '''
        Get the usage sequences at each Return node
        '''
        token_values = get_code_tokens(node, [arg_tuple[0] for arg_tuple in self.arg_status_list], self.source_file)
        if token_values:
            self.seq_map['return'].append(list(token_values))
        self.generic_visit(node)


    def visit_FunctionDef(self, node):
        '''
        Call to a helper function to gather function information
        '''
        self._helper_func_visit(node)

    def visit_AsyncFunctionDef(self, node):
        '''
        Call to a helper function to gather function information
        '''
        self._helper_func_visit(node)

    def _helper_func_visit(self,node):
        '''
        Gather function based information, once all the children nodes have been visited
        it then collates the information gathered into StaticInformationHolders and placed into a map
        '''
        # Get the tokens for the comments
        current_docstring = get_docstring_tokens(node)
        function_name = node.name
        if node.returns is not None:
            with open(self.source_file, 'r') as s_file:
                source = s_file.read()
                return_type = str(ast.get_source_segment(source, node.returns))
        else:
            return_type = None
        arguments = node.args
        #Get all the argument names, needed to avoid tokenizing non-argument Name tokens
        for arg in arguments.posonlyargs:
            self._helper_visit_arg(arg)
        for arg in arguments.args:
            self._helper_visit_arg(arg)
        if arguments.vararg is not None:
            self._helper_visit_arg(arguments.vararg)

        #Visit all the child nodes (dfs)
        self.generic_visit(node)

        #All the child nodes have been visited, so now we can populate the static_camp
        arg_list = []

        #Normalize all the argument going into arg_name list
        for arg_tuple in self.arg_status_list:
            for a in norm(arg_tuple[0]).split(" "):
                arg_list.append(a)

        #Create a static information holder - used for all this functions ReturnInformationHolder and ArgumentInformationHolder types
        sif = StaticInformationHolder(function_name, current_docstring, arg_list)

        #Populate the argument information holders for each arg_info
        for (arg, ty, ln, col) in self.arg_status_list:
            current_arg = ArgumentInfo(sif, arg, self.seq_map[arg])
            current_arg.set_error_locs([(ln, col)]) #TODO: Also include NAME locations
            #The argument already had a type
            if ty is not None:
                current_arg.set_type(ty)
                self.static_map['closed_args'].append(current_arg)
            else:
                self.static_map['open_args'].append(current_arg)
 
        #If the function contains a return statement, create an ReturnInformationHolder
        if 'return' in self.seq_map:
            return_inf = ReturnInfo(sif, self.seq_map['return'])
            return_inf.set_error_locs([(node.lineno, node.end_col_offset)]) #TODO: Also include RETURN locations
            if return_type is not None:
                return_inf.set_type(return_type)
                self.static_map['closed_returns'].append(return_inf)
            else:
                self.static_map['open_returns'].append(return_inf)
