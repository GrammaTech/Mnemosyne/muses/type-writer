class StaticInformationHolder:
    '''
    A holder of the static information extracted common to arguments and returns
    '''
    def __init__(self, f_name, comment, arg_list):
        self.comment = comment
        self.f_name = f_name
        self.arg_list = arg_list
        self.type = None

    def set_type(self, t):
        self.type = t

    def set_vector(self, vector):
        self.vector = vector

    def set_error_locs(self, error_locs):
        self.error_locs = error_locs

class ArgumentInfo(StaticInformationHolder):
    '''
    A holder of the static information unique to arguments
    '''
    def __init__(self, parent_info, arg_name, usage_seqs):
        self.__dict__.update(parent_info.__dict__)
        self.arg_name = arg_name
        self.usage_seqs = usage_seqs


class ReturnInfo(StaticInformationHolder):
    '''
    A holder of the static information unique to function returns
    '''
    def __init__(self, parent_info, return_seqs):
        self.__dict__.update(parent_info.__dict__)
        self.return_seqs = return_seqs
