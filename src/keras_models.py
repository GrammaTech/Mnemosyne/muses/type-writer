from keras.models import model_from_json

from keras.models import Model
from keras.layers import LSTM
from keras.layers import Bidirectional
from keras.layers import Dense
from keras import Input
from numpy import array, zeros
import numpy as np
from sklearn.preprocessing import LabelEncoder

from gensim.models import Word2Vec
import json
import logging
import pickle

ARG_MODEL_W_LOC = "src/model/arg_model.h5"
ARG_MODEL_S_LOC = "src/model/arg_model.json"
ARG_LABEL_E_LOC = "src/model/arg_encoder.pkl"

RET_MODEL_W_LOC = "src/model/ret_model.h5"
RET_MODEL_S_LOC = "src/model/ret_model.json"
RET_LABEL_E_LOC = "src/model/ret_encoder.pkl"

#Load in all the models once on import

arg_json_file = open(ARG_MODEL_S_LOC)
loaded_arg_model_json = arg_json_file.read()
arg_json_file.close()
arg_model = model_from_json(loaded_arg_model_json)
arg_model.load_weights(ARG_MODEL_W_LOC)

arg_encoder = pickle.load(open(ARG_LABEL_E_LOC,"rb"))

ret_json_file = open(RET_MODEL_S_LOC)
loaded_ret_model_json = ret_json_file.read()
ret_json_file.close()
ret_model = model_from_json(loaded_ret_model_json)
ret_model.load_weights(RET_MODEL_W_LOC)

ret_encoder = pickle.load(open(RET_LABEL_E_LOC,"rb"))

CODE_MODEL_LOC="src/model/w2v_code_model.bin"
WORD_MODEL_LOC="src/model/w2v_word_model.bin"

code_model = Word2Vec.load(CODE_MODEL_LOC)
word_model = Word2Vec.load(WORD_MODEL_LOC)

W2V_LENGTH = 100

def get_prob_for_types(vector, arg=True):
    '''
    From a vector, get the predicted probability classifications
    '''
    if arg:
        model = arg_model
        encoder = arg_encoder
    else:
        model = ret_model
        encoder = ret_encoder

    data = vector.reshape(1,72,100)
    logging.debug("Predicting")
    prob = model.predict(data, batch_size=1)
    logging.debug("Predicted")
    sorted_prob = (-prob).argsort()
    sorted_ = sorted_prob[0][:4]

    label_sort = list(filter(lambda a: a != "unknown", encoder.inverse_transform(sorted_)))

    rank = 0;
    prediction_dict = {}
    for t in label_sort:
        prediction_dict[t] = rank
        rank = rank + 1
    return prediction_dict
   
class IdentifierEmbedding:

    def __init__(self, arg_name, args_name, func_name):

        self.model = code_model
        self.arg_name = arg_name
        self.args_name = args_name
        self.func_name = func_name

    def seq_length_param(self):

        return {
            "arg_name": 10,
            "sep": 1,
            "func_name": 10,
            "args_name": 10
        }

    def seq_length_return(self):

        return {
            "func_name": 10,
            "sep": 1,
            "args_name": 10,
            "padding": 10
        }

    def generate_datapoint(self, seq_length):

        datapoint = np.zeros((sum(seq_length.values()), W2V_LENGTH))
        separator = np.ones(W2V_LENGTH)

        p = 0
        for seq, length in seq_length.items():

            if seq == "sep":

                datapoint[p] = separator
                p += 1

            elif seq == 'padding':

                for i in range(0, length):
                    datapoint[p] = np.zeros(W2V_LENGTH)
                    p += 1
            else:

                try:

                    for w in vectorize_string(self.__dict__[seq], length, self.model):
                        datapoint[p] = w
                        p += 1
                except AttributeError:
                    pass

        return datapoint

    def param_datapoint(self):

        return self.generate_datapoint(self.seq_length_param())

    def return_datapoint(self):

        return self.generate_datapoint(self.seq_length_return())


class TokenEmbedding:

    def __init__(self, len_tk_seq, num_tokens_seq, args_usage, return_expr):
        self.model = code_model
        self.len_tk_seq = len_tk_seq
        self.num_tokens_seq = num_tokens_seq
        self.args_usage = args_usage
        self.return_expr = return_expr

    def param_datapoint(self):

        datapoint = np.zeros((self.num_tokens_seq*self.len_tk_seq, W2V_LENGTH))

        p = 0
        for i, u in enumerate(self.args_usage):
            if i >= self.num_tokens_seq:
                break
            for w in vectorize_string(u, self.len_tk_seq, self.model):
                datapoint[p] = w
                p += 1

        return datapoint

    def return_datapoint(self):

        datapoint = np.zeros((self.num_tokens_seq*self.len_tk_seq, W2V_LENGTH))

        if isinstance(self.return_expr, str):
            p = 0
            for w in vectorize_string(self.return_expr, self.len_tk_seq, self.model):
                datapoint[p] = w
                p += 1

        return datapoint


class CommentEmbedding:

    def __init__(self, comment):
        self.model = word_model
        self.comment = comment

    def seq_length_param(self):

         return {
            "comment": 20,
            }

    def seq_length_return(self):

        return {
            "comment": 20,
            }

    def generate_datapoint(self, seq_length):

        datapoint = np.zeros((sum(seq_length.values()), W2V_LENGTH))
        separator = np.ones(W2V_LENGTH)

        p = 0
        for seq, length in seq_length.items():

            try:

                for w in vectorize_string(self.__dict__[seq], length, self.model):
                    datapoint[p] = w
                    p += 1
            except AttributeError:
                pass

        return datapoint

    def param_datapoint(self):

        return self.generate_datapoint(self.seq_length_param())

    def return_datapoint(self):

        return self.generate_datapoint(self.seq_length_return())

def vectorize_string(sentence: str, feature_length: int, w2v_model: Word2Vec) -> np.ndarray:
    """
    Vectorize a sentence to a multi-dimensial NumPy array
    Roughly based on https://github.com/sola-da/NL2Type/blob/master/scripts/csv_to_vecs.py
    """

    vector = np.zeros((feature_length, W2V_LENGTH))

    if type(sentence) is str:
        sentence = sentence.split()
    for i, word in enumerate(sentence):
        if i >= feature_length:
            break
        try:
            vector[i] = w2v_model.wv[word]
        except KeyError:
            pass

    return vector
