import nltk
nltk.download('punkt')
nltk.download('wordnet')

from nltk.stem import WordNetLemmatizer
import re

def norm(ident):
   '''
   For a given string identifier, this function tokenizezes, lemmatizes, and converts it to lower case
   '''
   if "_" in ident:
       ident = re.sub('(\w)_(\w)',r'\1 \2',ident)
   split_camel = (re.sub('([A-Z][a-z]+)', r' \1', re.sub('([A-Z]+)', r' \1', ident)).split())

   word_list = nltk.word_tokenize(' '.join(split_camel))

   lemmatizer = WordNetLemmatizer()
   join_list = ' '.join(word_list)
   normalized_ident = lemmatizer.lemmatize(join_list.lower())
   return normalized_ident 
