from pathlib import Path
import ast
from src.static_info_collater import StaticCollater
from src.util import norm
from src.keras_models import TokenEmbedding, IdentifierEmbedding, CommentEmbedding, get_prob_for_types
import logging
import numpy as np

ID_SEQ_LEN = 10
TOK_SEQ_LEN = 21
COM_SEQ_LEN = 20

def type_writer(path_to_source: Path):
    '''
    Takes a source code path and returns a type predeiction
    '''
    with open(path_to_source, 'r') as source:
        type_map = {}
        tree = ast.parse(source.read(), mode='exec', type_comments=True)
        logging.debug('Parsed file')
        for node in ast.walk(tree):
            # Find predictions for function arguments and return types at the function node level
            if isinstance(node, ast.FunctionDef) or isinstance(node, ast.AsyncFunctionDef):
                # Gets all the information at the function level
                s_collate = StaticCollater(path_to_source)
                s_collate.visit(node)
                static_inf_map = s_collate.get_static_info()
                open_args = static_inf_map["open_args"]
                # Get predictions for arguments
                for arg_info in open_args:
                   # Create sequences and vectors from the static information
                   id_embedding = IdentifierEmbedding(norm(arg_info.arg_name), " ".join(arg_info.arg_list), norm(arg_info.f_name))
                   ident_vector = id_embedding.param_datapoint()
                   #ident_vector = get_v_from_RNN(ident_sequence)
                   token_embedding = TokenEmbedding(7, 3, arg_info.usage_seqs, None)
                   token_vector = token_embedding.param_datapoint()
                   #acode_vector = get_v_from_RNN(code_token_sequence)
                   doc_string_sequence = arg_info.comment[:COM_SEQ_LEN]
                   comment_embedding = CommentEmbedding(" ".join(doc_string_sequence))
                   comment_vector = comment_embedding.param_datapoint()
                   #doc_vector = get_v_from_RNN(doc_string_sequence, code=False)
                   # concatentate the vectors and predicts the type probabilities       
                   fin_vector = concat_vectors(ident_vector, token_vector, comment_vector)
                   logging.debug("Getting probabilities for %s argument(s)", len(fin_vector))
                   type_prob = get_prob_for_types(fin_vector)
                   logging.debug("Got probabilities for arguments")
                   if arg_info.f_name not in type_map.keys():
                       type_map[arg_info.f_name] = {}
                   type_map[arg_info.f_name][arg_info.arg_name] = [type_prob, arg_info.error_locs[0]]
                open_rets = static_inf_map["open_returns"]
                # Get predictions for returns
                for ret_info in open_rets:
                   # Create sequences and vectors from the static information
                   id_embedding = IdentifierEmbedding(None, " ".join(ret_info.arg_list), norm(ret_info.f_name))
                   ident_vector = id_embedding.return_datapoint()
                   #ident_vector = get_v_from_RNN(ident_sequence)
                   token_embedding = TokenEmbedding(7, 3, None, ret_info.return_seqs)
                   token_vector = token_embedding.return_datapoint()
                   #code_vector = get_v_from_RNN(code_token_sequence)
                   doc_string_sequence = ret_info.comment[:COM_SEQ_LEN]
                   comment_embedding = CommentEmbedding(" ".join(doc_string_sequence))
                   comment_vector = comment_embedding.return_datapoint()
                   #doc_vector = get_v_from_RNN(doc_string_sequence, code=False)
                   #concatenates the vectors and predicts the type probabilities
                   fin_vector = concat_vectors(ident_vector, token_vector, comment_vector)
                   logging.debug("Getting probabilities for % return type(s)", len(fin_vector))
                   type_prob = get_prob_for_types(fin_vector, False)
                   logging.debug("Got probabilities for return types")
                   if ret_info.f_name not in type_map.keys():
                       type_map[ret_info.f_name] = {}
                   type_map[ret_info.f_name]["return"] = [type_prob, ret_info.error_locs[0]]
    return type_map
                    

def concat_vectors(ident_vector, code_vector, doc_vector):
    '''
    Concatenates three vectors into one for prediction
    '''
    return np.concatenate((ident_vector,code_vector,doc_vector))
