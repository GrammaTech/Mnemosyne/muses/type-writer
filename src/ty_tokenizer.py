from typing import List
from collections import deque
from ast import AST
from math import ceil
from pathlib import Path
from tokenize import generate_tokens
import linecache
from src.util import norm
import ast

BUFFER_SIZE = 7
MID_POINT = ceil(BUFFER_SIZE/2)
DOC_BUFFER_SIZE = 20

    
def populate_deque(token_deque, s_file, line_no, current_pos, name=None, rev=False):
    '''
    A helper function which takes a source file and line number and reads it 
    in so that the tokenize class can parse it.

    It then populates a deque of tokens so that name is in the center

    Since the code is so similar, it also handles padding the ends or beginning
    of the deque if needed

    :token_deque: The deque to be filled
    :s_file: The source file to be read
    :line_no: The line number to be read
    :current_pos, the current position of the named variable in the deque, 
    0 if it hasn't been populated yet
    
    :name: Name to be looked for, None if already found
    :rev: boolean variable that is true if the function is preprending to token_deque
    :returns: a deque and the current position of the named variable
    '''
    #The python tokenize class only takes a readline() object, so we need to grab 
    #all the tokens at line_no by going through the file
    with s_file.open() as f:
      current_list = []
      for five_tuple in generate_tokens(f.readline):
          if five_tuple.start[0]== line_no:
              current_list.append(five_tuple)
      if rev:
         tokenization = reversed(current_list)
      else:
         tokenization = current_list

      #Populate the token_deque so that the identifier is in the center
      #Because a line might have fewer than the needed tokens on either side, 
      #we may be processing the 
      #tokens in reverse order in the previous lines
      for token_tuple in tokenization:
        norm_token = norm(token_tuple.string)
        if token_tuple.type == 5 or token_tuple.type == 6: 
           continue
        if not rev:
           if current_pos == MID_POINT and len(token_deque) < BUFFER_SIZE:
               token_deque.append(norm_token)
           elif name is not None and name in token_tuple.string and current_pos == 0:
               token_deque.append(norm_token)
               current_pos = len(token_deque)
           elif current_pos == 0:
               token_deque.append(norm_token)
               if len(token_deque) == MID_POINT:
                   token_deque.popleft()
           elif current_pos < MID_POINT and len(token_deque) < (current_pos + MID_POINT - 1):
               token_deque.append(norm_token)
           else: break;

           while len(token_deque) >= MID_POINT:
               token_deque.popleft()
        else:
           if len(token_deque) < BUFFER_SIZE:
               token_deque.appendleft(norm_token)
    return token_deque, current_pos
    

def get_code_tokens(node: AST, arg_list, source: Path) -> deque:
    '''
    Given a function_body abstract syntax tree and a name, finds the tokenizations 
    of the source code of length BUFFER_SIZE and the tokenization of the return sequence

    If iterates over all the children in the function until it finds a name which 
    matches the variable name given, then gets the source code line. It tokenizes 
    this line and then calls a helper function to populate the buffer around the 
    code occurrence. If the tokens around the occurrence do not populate the buffer, 
    tokenize the appropriate line above or below the occurence to do so.

    NOTE: does not currently handle multiple names on the same line, will need to 
    add column tracking to do so.

    :body_ast: AST of a function body
    :name: name of the variable we want occurrences for
    :source: Path to the source file

    :returns: a map from variable name to code_occurrence tokenizations
    '''
    code_occurrence = []
    if isinstance(node,ast.Name):
        if node.id in arg_list or len(arg_list) == 0:
            line_no = node.lineno
            l_deque = deque()
            l_deque, current_pos = populate_deque(l_deque, source, line_no, 0, node.id)

            if len(l_deque) < BUFFER_SIZE:
                if len(l_deque) < (current_pos+MID_POINT-1):
                   line_no = line_no+1
                   l_deque, current_pos = populate_deque(l_deque, source, line_no, current_pos)
                else:
                   line_no = line_no-1
                   l_deque, current_pos = populate_deque(l_deque, source, line_no, current_pos, rev=True)
            code_occurrence = l_deque
    elif isinstance(node, ast.Return):
       with source.open() as f:
          line_no = node.lineno
          current_list = []
          for five_tuple in generate_tokens(f.readline):
              if five_tuple.start[0]== line_no:
                 current_list.append(five_tuple)
          return_tokens = [tok.string for tok in current_list]
          code_occurrence = return_tokens
    return code_occurrence

def get_docstring_tokens (function_ast: AST) -> List[str]:
    '''
    Given a function ast, returns a list of the first 20 words in the 
    docstring for that function

    :function_ast: an AST of a function
    :returns: a List of strings representing words in the docstring comment
    '''
    docstring = ast.get_docstring(function_ast, clean=True)
    if docstring is None:
        return[""]
    doc_split = docstring.split(" ")
    return doc_split[0:DOC_BUFFER_SIZE]
