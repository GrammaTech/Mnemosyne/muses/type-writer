#!/usr/bin/env python3

"""
JSON-RPC entrypoint.
"""

from http.server import BaseHTTPRequestHandler
from kitchensink.utility.forkinghttpserver import ForkingHTTPServer
import io
from jsonrpcserver import dispatch, method
from socketserver import ForkingMixIn
import json
import logging
import os
import pathlib
import subprocess
import sys
import tempfile


class TypeWriterHttpServer(BaseHTTPRequestHandler):
    protocol_version = 'HTTP/1.1'  # to allow persistent connections

    def do_POST(self):
        # Process request
        request = self.rfile.read(int(self.headers["Content-Length"])).decode()
        response = dispatch(request)
        content = str(response).encode()
        # Return response
        self.send_response(response.http_status)
        self.send_header("Content-Length", len(content))
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        self.wfile.write(content)

@method
def run_type_writer(*, source=''):
    # We wait until we're actually in the child process to load the
    # models, because Keras/Tensorflow is not fork-safe.
    from src.type_writer import type_writer
    with tempfile.NamedTemporaryFile() as temp:
        logging.debug('Wrote temporary file')
        temp.write(bytes(source, encoding='utf-8'))
        temp.seek(0)
        logging.debug('Computing type predictions')
        type_predictions = type_writer(pathlib.Path(temp.name))
        return {"prediction list": json.dumps(type_predictions)}


if __name__ == "__main__":
    # to see the port number
    logging.basicConfig(level=logging.INFO)
    port = int(os.environ['PORT'] or '4000') if 'PORT' in os.environ else 4000
    logging.info(" * Listening on port %s", port)
    with ForkingHTTPServer(("0.0.0.0", port), TypeWriterHttpServer) as s:
        s.serve_forever()
