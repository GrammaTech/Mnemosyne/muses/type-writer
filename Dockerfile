FROM ubuntu:20.04

RUN apt-get -y update && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --no-install-recommends curl \
    git python3-pip r-base

COPY . /root/typewriter/
WORKDIR /root/typewriter/
RUN pip3 --no-cache-dir install -r requirements.txt && \
    git submodule update --init --recursive && \
    apt purge -y git curl && \
    apt autoremove --purge -y && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*
