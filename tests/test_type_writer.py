from src.type_writer import type_writer
from pathlib import Path

def test_type_writer():
    source_path = Path.cwd() / 'tests' / 'source_test_code.py'
    predictions = type_writer(source_path)
    #assert(predictions['find_match']['color'][0]['str'] != 4)


if __name__ == "__main__":
    test_type_writer()
