def find_match(color):
    """
    Args:
    color (str): color to match on and return
    """
    candidates = get_colors()
    for candidate in candidates:
      if color == candidate:
        return color
    return None


def get_colors():
    return ["red", "blue", "green"]
